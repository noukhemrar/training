package com.khemra.recycler_learning1.models;

public class MyListData {
    private String detail;
    private int imgID;

    public MyListData(String detail, int imgID){
            this.detail = detail;
            this.imgID = imgID;
    }

    public String getDetail() {
        return detail;
    }

    public int getImgID() {
        return imgID;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setImgID(int imgID) {
        this.imgID = imgID;
    }

}
