package com.khemra.recycler_learning1.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.khemra.recycler_learning1.R;
import com.khemra.recycler_learning1.adapters.MyListAdapter;
import com.khemra.recycler_learning1.models.MyListData;

public class MainActivity extends AppCompatActivity {

    private MyListAdapter myLisTAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyListData[] myListData = new MyListData[]{
                new MyListData("Email", android.R.drawable.ic_dialog_email),
                new MyListData("Info", android.R.drawable.ic_dialog_info),
                new MyListData("Delete", android.R.drawable.ic_delete),
                new MyListData("Dialer", android.R.drawable.ic_dialog_dialer),
                new MyListData("Alert", android.R.drawable.ic_dialog_alert),
                new MyListData("Map", android.R.drawable.ic_dialog_map),
                new MyListData("Map", android.R.drawable.ic_dialog_map),
                new MyListData("Map", android.R.drawable.ic_dialog_map),
                new MyListData("Map", android.R.drawable.ic_dialog_map),
                new MyListData("Map", android.R.drawable.ic_dialog_map),
                new MyListData("Map", android.R.drawable.ic_dialog_map),
                new MyListData("Map", android.R.drawable.ic_dialog_map),
        };

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler);
        myLisTAdapter = new MyListAdapter(getBaseContext());
        myLisTAdapter.addMyListAdapter(myListData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(myLisTAdapter);


    }
}